const joi = require('joi');

const userDatavalidator = joi.object({
    firstName: joi.string().optional(),
    lastName: joi.string().optional(),
    Email: joi.string().required().error(new Error('Email is mendatory')),
    password: joi.string().required().error(new Error('password is mendatory')),
})

module.exports = {
    userDatavalidator,
}