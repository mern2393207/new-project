// methods.js
// importing all the packages and required files
const express = require('express');
const {Data1} = require('../Model/userModel');
const {Data2} = require('../Model/photomodel')
const {userDatavalidator} = require('../Validator/validator')
const bcrypt = require('bcrypt');
const secretKey = "secretKey";
const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require("uuid");
const multer = require('multer');
const path = require('path');
const { error } = require('console');


const router = express();
router.use(express.json());


// this is for checking - working the structure
const get = async(req, res) => {
    res.send("hello world");
}


// this is for creating the data in database
const postuser= async(req, res) => {
    try {
        await userDatavalidator.validateAsync(req.body);
        const {
            Email, password
        } = req.body;

        const exist = await Data1.findOne({$or: [{ Email }, { password }]});
        if(exist) {
            return res.send('user alredy present');
        }
        else {
            const hashPass = await bcrypt.hash(req.body.password, 10);
            req.body.password = hashPass;
            await Data1.create(req.body);
            return res.send("user created");
        }
    } catch (error) {
        console.error("Error creating user", error);
        return res.json({ error: error.message });
    }
}


// login the user
const Login = async(req, res) => {
    try {
        const {
            Email, password
        } = req.body;
        const user = await Data1.findOne({Email});
        if(!user) {
            return res.send('user is not present');
        }
        const passvalid = await bcrypt.compare(password, user.password);
        if(passvalid) {
            const jti = uuidv4(); 
            await Data1.updateOne({ _id: user._id }, { $set: { jti: jti } });

            jwt.sign({userId: user._id, jti}, secretKey, {expiresIn: '300s'}, (error, token) => {
                if(error) {
                    console.error("error in genrating the token", error);
                    return res.send("server error");
                }
                return res.send({token});
            })
        }
        else {
            return res.send('check password, password is invalid');
        }
    } catch (error) {
        return res.send("server error");
    }
}


// validation of profile
const profileValid = async (req, res) => {
    jwt.verify(req.token, secretKey, async (err, authData) => {
      if (err) {
        res.send({ msg: "invalid token" });
      } else {
        try {
            const userData = await Data1.findOne({ _id: authData.userId }, { password: 0});

          if (!userData) {
            return res.status(400).json({ msg: "user not found" });
          }
          if (userData.jti !== authData.jti) {
            return res.status(400).json({ msg: "Invalid token" });
          }
          res.json({
            msg: "Profile accessed",
            user: userData
          });
        } catch (error) {
          console.error(error);
          res.status(500).json({ msg: "Internal server error" });
        }
      }
    });
};
  

// this is for correct file reading not binary
const storage = multer.diskStorage({
  destination: path.join(__dirname, '../upload'),
  filename: function(req, file, cb) {
    cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 1
  }
});

const fileSecure = async (req, res) => {
  jwt.verify(req.token, secretKey, async (err, authData) => {
    if (err) {
      return res.send('Invalid token');
    }
    try {
      const userData = await Data1.findOne({ _id: authData.userId }, { password: 0 });
      if (!userData) {
        return res.send('User not found');
      }
      if (userData.jti !== authData.jti) {
        return res.send('Invalid token');
      }
      const data2 = new Data2({ userId: userData._id });
      await data2.save();
      
      upload.single('file')(req, res, function(err) {
        if (err instanceof multer.MulterError) {
          return res.send('File upload error');
        } else if (err) {
          return res.send('Server error');
        }
        return res.status(200).json({ msg: "File uploaded successfully", filename: req.file.filename });
      });

    } catch (error) {
      console.error(error);
      res.send('Server error');
    }
  });
};



// another post using aggregate
const aggregationpost = async (req, res) => {
  jwt.verify(req.token, secretKey, async (err, authData) => {
    if (err) {
      return res.send("Invalid token");
    }
    try {
      const userData = await Data1.findOne({ _id: authData.userId }, { password: 0 });
      if (!userData) {
        return res.send('User not found');
      }
      if (userData.jti !== authData.jti) {
        return res.send('Invalid token');
      }
      
      const userId = authData.userId; 
      const text = req.body.text;

      await Data2.updateOne({ userId: userId }, { $set: { text: text } }, { upsert: true });

      res.send('Text message updated in Data2 collection successfully');
    } catch (error) {
      console.error(error);
      res.send('Server error');
    }
  });
};

// for delete the comment we enter this will unset that
const aggregationdelete = async (req, res) => {
  jwt.verify(req.token, secretKey, async (err, authData) => {
      if (err) {
          return res.send("Invalid token");
      }
      try {
          const userData = await Data1.findOne({ _id: authData.userId }, { password: 0 });
          if (!userData) {
              return res.send('User not found');
          }
          if (userData.jti !== authData.jti) {
              return res.send('Invalid token');
          }
          const userId = req.params._id;
          
          await Data2.updateOne({ userId: userId }, { $unset: { text: "" } });
          
          res.send('Text field deleted successfully');
      } catch (error) {
          console.error(error);
          res.send('Server error');
      }
  });
};






module.exports = {
    get,
    postuser,
    Login,
    profileValid,
    fileSecure,
    aggregationpost,
    aggregationdelete,
}