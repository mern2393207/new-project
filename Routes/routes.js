// routes.js

const express = require('express');

const methods = require('../Controller/methods');
const verifyToken = require('../Validator/tokenvalidator');


const router = express.Router();

router.get('/', methods.get );
router.post('/postuser', methods.postuser);
router.post('/Login', methods.Login);
router.post('/profileValid', verifyToken, methods.profileValid);
router.post('/fileSecure', verifyToken, methods.fileSecure);
router.post('/aggregationpost/:_id', verifyToken, methods.aggregationpost);
router.put('/aggregationdelete/:_id', verifyToken, methods.aggregationdelete);


module.exports = router;