const express = require('express');
const mongoose = require( 'mongoose' );
const router = require('./Routes/routes')
const port = 3000;
const app = express();


app.use(express.json());
app.use(router);



app.listen(port, () => {
    mongoose.connect('mongodb://0.0.0.0:27017/Assensment');
    console.log(`Server is running on port ${port}`);
})