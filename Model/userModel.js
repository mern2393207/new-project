const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
    },
    lastName: {
        type: String,
    },
    Email: {
        type: String,
    },
    password: {
        type: String,
    },
    jti: {
        type: String,
    }
})

const Data1 = mongoose.model('Data1', userSchema);

module.exports = {
    Data1,
}