// photomodel.js


const { number } = require('joi');
const mongoose = require('mongoose');

const photoSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Data1'
    },
    text: {
        type: String,
    }
})

const Data2 = mongoose.model('Data2', photoSchema);

module.exports = {
    Data2,
}